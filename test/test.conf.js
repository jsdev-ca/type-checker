// TODO: Rename
export const falsy = [];

export const keys = [
  'isArray',
  'isBoolean',
  'isDate',
  'isFunction',
  'isNull',
  'isNumber',
  'isObject',
  'isRegExp',
  'isString',
  'isSymbol',
  'isUndefined'
];

// TODO: Rename
export const truthy = [
  {
    testCase: [],
    testCaseStringified: '[]'
  },
  {
    testCase: true,
    testCaseStringified: 'true'
  },
  {
    testCase: new Date(),
    testCaseStringified: 'new Date()'
  },
  {
    testCase: function () {},
    testCaseStringified: 'function () {}'
  },
  {
    testCase: null,
    testCaseStringified: 'null'
  },
  {
    testCase: 1,
    testCaseStringified: '1'
  },
  {
    testCase: {},
    testCaseStringified: '{}'
  },
  {
    testCase: /^[a-z]$/,
    testCaseStringified: '/^[a-z]$/'
  },
  {
    testCase: 'a',
    testCaseStringified: 'a'
  },
  {
    testCase: Symbol('foo'),
    testCaseStringified: 'Symbol(\'foo\')'
  },
  {
    testCase: undefined,
    testCaseStringified: 'undefined'
  }
];
