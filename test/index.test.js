import chai from 'chai';
import {
  falsy,
  keys,
  truthy
} from './test.conf';
import typeChecker from '../src/index';

const should = chai.should();

describe('typeChecker', () => {
  it(`should be an object that has all of these keys: ${keys.join(', ')}`, () => {
    typeChecker.should.be.an('object').that.has.all.keys(keys);
  });

  keys.forEach((key, index) => {
    describe(`.${key}()`, () => {
      it('should be a function', () => {
        typeChecker[key].should.be.a('function');
      });

      it(`should return true when its argument is \`${truthy[index].testCaseStringified}\`.`, () => {
        typeChecker[key](truthy[index].testCase).should.be.true;
      });
    });
  });
});
