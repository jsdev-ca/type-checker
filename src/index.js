const typeChecker = (() => {
  const types = 'Array Boolean Date Function Null Number Object RegExp String Symbol Undefined'.split(' ');
  const methods = {};

  function getType(value) {
    return Object.prototype
      .toString
      .call(value)
      .slice(8, -1)
      .toLowerCase();
  }

  types.forEach(type => {
    methods[`is${type}`] = value => getType(value) === type.toLowerCase();
  });

  return methods;
})();

export default typeChecker;
